//music.frag.inc: Music parameters.
<?# Don't forget to update gen-gfx.py when you change this! ?>
<? my $bpm = 116.0; my $startofs_beats = 0; #19; ?>

<?#----------------------------------------------?>

<?  use Math::Trig; use POSIX; ?>

<?  # Computed values
    my $bps = $bpm/60.0; my $spb = 60.0/$bpm;
    sub pf { return sprintf("%.20f",shift); }   # convenience - Print Float
?>

#define BPS (<?= pf($bps) ?>)
    // beats per sec = BPM/60 (110 bpm)
#define SPB (<?= pf($spb) ?>)
    // sec per beat, precomputed
#define BPP (2048.0)
    // beats per pattern - make it longer than the song
    // if you don't want repeats

// DEBUG: where in the demo you want to start.
#define START_OFS_SEC <?= pf($startofs_beats * $spb) ?>

<?# vi: set ts=4 sts=4 sw=4 et ai foldmethod=marker foldenable foldlevel=0 ft=glsl330: ?>

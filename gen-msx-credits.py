#!/usr/bin/python3
# gen-msx-credits.py for ncl02.  By cxw/Incline.
# This file only works in beats.  The bpm are defined in music.frag.inc.
import csv, pdb
from textwrap import indent, dedent

# *** Make sure these match music.frag.inc!
bpm = 116
bps = bpm/60.0
spb = 60.0/bpm

songinfo="""\
// Christmas, CM.  Transcribed by cxw from Harmonia Sacra, 1812,
// arr. from Handel.  https://hymnary.org/media/fetch/106008"""

nsharps = 2    # key, defined by # of sharps or minus number of flats

import scales

# Move all the notes for this key into globals for convenience
globals().update(scales.freqs(nsharps))
# Tuple listing all the notes we know about.  These are used to generate
# the GLSL constants for the notes.  r must always be first.
notes=(r, c4, d4, e4, f4, g4, a4, b4, c5, d5, e5, f5, g5, a5, b5, c6, d6);

# Reverse map
pitchnum_by_note = {}
for noteidx, note in enumerate(notes):
    pitchnum_by_note[note] = noteidx

# Convenience constants
e=0.5;      # save typing - Eighth note (half-beat).
de=1.5*e;   # dotted eighth
s=0.25;     # Sixteenth

tiny = s*0.25;

# music: list of ("note", duration in beats[, True]).
# If the True is specified, this is a boundary between parts.  The first
# entry is always assumed to be a boundary.

# A useful data-entry command for vim:
# 'a,.g!/#/s/\(\w\+\)\s\+\([0-9a-zA-Z.]\+\)/(\1, \2),

beats_per_measure = 4

# In 4/4, two voices.  melody is the upper voice.
# Durations are in beats (1 = quarter note).
credz_voice1=[
    # Credits
    # Line 1
    (r, 3, True),
    (f4, de),
    (g4, s),

    (a4, 1),
    (d5, 1),
    (c5, 1),
    (b4, 1),

    (a4, 1),
    (d4, de),
    (e4, s),
    (f4, 1-tiny),
    (r,tiny),
    (f4, de),
    (g4, s),

    (a4, 1-tiny),
    (r,tiny),
    (a4, 1-tiny),
    (r,tiny),
    (a4, 1),
    (g4, e),
    (f4, e-tiny),
    (r,tiny),

    (f4, 1),
    (e4, 2-tiny),
    (r,tiny),
    (c5, e),
    (d5, e),

    # Line 2
    (e5, 1),
    (a4, 1),
    (g4, 1-tiny),
    (r,tiny),
    (g4, 1-tiny),
    (r,tiny),

    (g4, 1),
    (f4, e),
    (e4, e),
    (f4, 1),
    (d5, e),
    (c5, e),

    (b4, 1),
    (a4, 1),
    (g4, 1),
    (f4, 1),

    (b4, 1),
    (a4, 2),
    (e5, 1),

    # Line 3
    (a4, 1),
    (d5, 1),
    (f4, 1),
    (e4, 1),

    (d4, 3),
    (r, 1),

    ] #credz_voice1

# Note: part-beginning markers (True) are ignored here for sync, but are
# used to tack on the repeat at the end.  Keep them lined up with the
# melody.
credz_voice2=[  # Alto
    # Credits
    # Line 1
    (r, 3, True),
    (d4, de),
    (c4, s),

    (d4, 1),
    (f4, 1),
    (e4, e),
    (f4, e),
    (g4, 1),

    (d4, 1),
    (d4, de),
    (c4, s),
    (d4, 1-tiny),
    (r,tiny),
    (d4, 1-tiny),
    (r,tiny),

    (d4, 1-tiny),
    (r,tiny),
    (d4, 1-tiny),
    (r,tiny),
    (d4, e),
    (f4, e),
    (e4, e),
    (d4,e-tiny),
    (r,tiny),

    (d4, 1),
    (c4, 2-tiny),
    (r,tiny),
    (a4, 1),

    # Line 2
    (a4, e),
    (g4, e),
    (f4, 1),
    (e4, 1),
    (c4, 1),

    (d4, 1-tiny),
    (r,tiny),
    (d4, 1-tiny),
    (r,tiny),
    (d4, 1),
    (f4, 1),

    (g4, 1),
    (f4, 1),
    (d4, 1-tiny),
    (r,tiny),
    (d4, 1-tiny),
    (r,tiny),

    (d4, 3),
    (e4, 1),

    # Line 3
    (f4, 1),
    (d4, de),
    (e4, s),
    (d4, 1),
    (c4, 1),

    (d4, 3),
    (r, 1),

    ] #credz_voice2

def finalize(dat):

    dat.extend(dat)
#    # TODO add repeats or rests if necessary - want 72 beats
#    # Where the repeat will start
#    startrepeatrow = len(dat)
#
#    # Find where line 3 begins - currently the sixth True
#    dat2 = ( (x + (False,))[0:3] for x in dat )
#        # By default, this note isn't the start of a part (False)
#
#    boundary_number = -1
#    repeat_start_row = -1
#    for (rowidx, (note, duration, is_part_boundary)) in enumerate(dat2):
#        if is_part_boundary:
#            boundary_number = boundary_number + 1
#            if boundary_number == 5:    #0-indexed
#                repeat_start_row = rowidx
#                break
#
#    # Tack on the repeats of lines 3 and 4
#    dat.extend(dat[repeat_start_row:-1])
#
#    # The repeat is not the start of a new part
#    dat[startrepeatrow] = (
#        dat[startrepeatrow][0],
#        dat[startrepeatrow][1],
#        False
#    )
#
#    # Credits part - need 72 beats.
#    # Actual data is in gen-msx-credits.py.
#    dat.append( (r, 72, True) )
#
#    # Endpart
#    endparts=[
#        # done
#        (r,1000,True),      #start of the endpart
#        (r,1,True)          #end of the endpart
#    ]
#
#    dat.extend(endparts)
# finalize()

finalize(credz_voice1)
finalize(credz_voice2)

###################################
# Utility routines
from util import *

###################################
# Emit the frequencies and periods

print("// Tuning, including REST for consistency (credz part)")

for (name, freq_hz) in notes:
    # CF and CP so they don't clash with the non-credz constants,
    # which are in a different key
    print("""\
#define CF_%s (%.3f)
#define CP_%s (%.20f)\
"""%(name, freq_hz, name, 1.0/freq_hz if freq_hz > 0 else 0))

# Map pitch values to freq, period.  Pitch is rest=0, and after that
print("""
// LUT function from integer pitch number to freq and period
// (credz part, whence _c_notefreq)
vec2 cnotefreq(in float pitch_num)
{
    vec2 retval = vec2(CF_REST, CP_REST);
""")

for (idx, (name, _)) in enumerate(notes):
    print("    retval = mix(retval, vec2(CF_%s,CP_%s), step(%.1f, pitch_num));"%
        (name, name, idx))

print("""
    return retval;
}
""")

###################################

# Preprocess music
credz_voice1.append((r,1000))     # always a rest at the end

part_boundaries=[]

# Print function header
def gen(name, dat, saveboundaries = False, sustain_control = False):

    # TODO make this a binary-search tree
    print(dedent("""\
    vec4 get_song_data_%s(in float beat_in_pattern)
    {
        vec4 retval; //(pitchnum, dynamics, startbeat, endbeat)
            // frequency < 1.0 => no sound
        retval = vec4(0.0);     // by default, no sound"""%name))
    if sustain_control: print("    // ** Dynamics => sustain control")

    # Print song data.  Track timing as we go.
    currbeat = 0.0

    music2 = ( (x + (False,))[0:3] for x in dat )
        # By default, this note isn't the start of a part (False)

    for (note, duration, is_part_boundary) in music2:
        pitchnum = pitchnum_by_note[note]
        # Pitch number, dynamic-level modifier, starting beat, ending beat

        if sustain_control:     # Exponential - True => sustain longer
            dynamic = 0.1 if is_part_boundary else 1.0
        else:
            dynamic = 1.0 if (currbeat % beats_per_measure < 0.01) else 0.9

        thisnote_vec4 = "vec4(%.1f, %.3f, %.3f, %.3f)"%(
                            pitchnum, dynamic, currbeat, currbeat+duration)
        print(id4("retval = mix(retval, %s, step(%.3f, beat_in_pattern));"%
            (thisnote_vec4, currbeat)))

        if saveboundaries and is_part_boundary:
            part_boundaries.append(currbeat)

        currbeat += duration
    # next note

    # Print function footer
    print(dedent("""\
        return retval;
    } //get_song_data_%s()
    """%name))
# gen()

print("""// Score
%s
"""%songinfo)

gen('credz_voice1', credz_voice1)
gen('credz_voice2', credz_voice2)

# Save any timing information.  Using csv in case I need to add items later.
if len(part_boundaries)>0:
    with open('msx-part-boundaries.csv','w', newline='') as fd:
        outfd = csv.writer(fd, quoting=csv.QUOTE_NONNUMERIC)
        for (idx, boundary) in enumerate(part_boundaries):
            outfd.writerow([boundary])  # Save the boundary for gen-gfx
                # These times are in beats

            # Also stash the boundary data in msx.frag, in seconds
            print("#define START_SECS_%d (%.20f)"%(idx, boundary/bps))
        # next part boundary

# vi: set ts=4 sts=4 sw=4 et ai: #

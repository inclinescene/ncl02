# Note definitions: name and frequency (Hz)
# Frequencies in Hz, equal-tempered, A440.
# From http://www.phy.mtu.edu/~suits/notefreqs.html

def freqs(nsharps):
    rv = {}
    # Note frequencies
    rv['r']=('REST',0.0)  # name, freq
    if nsharps==6: # six sharps - F# maj (F#, G#, A#, B, C#, D#, F (=E#))
        rv['c4']=('C4',277.18)    # middle C
            # actually C#4, and likewise throughout.
        rv['d4']=('D4',311.13)
        rv['e4']=('E4',349.23)
        rv['f4']=('F4',369.99)
        rv['g4']=('G4',415.30)
        rv['a4']=('A4',466.16)
        rv['b4']=('B4',493.88)
        rv['c5']=('C5',554.37)
        rv['d5']=('D5',622.25)
        rv['e5']=('E5',698.46)
        rv['f5']=('F5',739.99)
        rv['g5']=('G5',830.61)
        rv['a5']=('A5',932.33)
        rv['b5']=('B5',987.77)
        rv['c6']=('C6',1108.73)
        rv['d6']=('D6',1244.51)

    elif nsharps==2:        # D maj
        rv['c4']=('C4',277.18)      # C#
        rv['d4']=('D4',293.66)
        rv['e4']=('E4',329.63)
        rv['f4']=('F4',369.99)      # F#
        rv['g4']=('G4',392)
        rv['a4']=('A4',440)
        rv['b4']=('B4',493.88)
        rv['c5']=('C5',554.37)      # C#
        rv['d5']=('D5',587.33)
        rv['e5']=('E5',659.25)
        rv['f5']=('F5',739.99)      # F#
        rv['g5']=('G5',783.99)
        rv['a5']=('A5',880)
        rv['b5']=('B5',987.77)
        rv['c6']=('C6',1108.73)     # C#
        rv['d6']=('D6',1174.66)

    elif nsharps==0:        # C maj
        rv['c4']=('C4',261.63)    # Middle C
        rv['d4']=('D4',293.66)
        rv['e4']=('E4',329.63)
        rv['f4']=('F4',349.23)
        rv['g4']=('G4',392)
        rv['a4']=('A4',440)
        rv['b4']=('B4',493.88)
        rv['c5']=('C5',523.25)
        rv['d5']=('D5',587.33)
        rv['e5']=('E5',659.25)
        rv['f5']=('F5',698.46)
        rv['g5']=('G5',783.99)
        rv['a5']=('A5',880)
        rv['b5']=('B5',987.77)
        rv['c6']=('C6',1046.5)
        rv['d6']=('D6',1174.66)

    elif nsharps==-1:        # F maj or d min
        rv['c4']=('C4',261.63)
        rv['d4']=('D4',293.66)
        rv['e4']=('E4',329.63)
        rv['f4']=('F4',349.23)
        rv['g4']=('G4',392)
        rv['a4']=('A4',440)
        rv['b4']=('B4',466.16)    # Bb
        rv['c5']=('C5',523.25)
        rv['d5']=('D5',587.33)
        rv['e5']=('E5',659.25)
        rv['f5']=('F5',698.46)
        rv['g5']=('G5',783.99)
        rv['a5']=('A5',880)
        rv['b5']=('B5',987.77)
        rv['c6']=('C6',1046.5)
        rv['d6']=('D6',1174.66)

    else:
        raise RuntimeError('Unknown key signature %d sharps'%nsharps)
    #endif nsharps

    return rv
# freqs()

# vi: set ts=4 sts=4 sw=4 et ai: #

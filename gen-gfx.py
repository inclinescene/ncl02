#!/usr/bin/python3
# gen-gfx.py: Print the message for ncl02.  cxw/incline.  CC-BY-SA 3.0.

import csv, sys

rate = 4    # chars per sec., default
BPS = 116.0/60.0    # bpm.  Must match the shaders (music.frag.inc)

# message_human is one line per part, each line being <description>/<timeval>/<text>
# <description> should be suitable to be uppercased to be a constant.
# <timeval> is how long the part lasts.
# if <timeval>=0, nchars/rate is used.
# if <timeval> < 0, -timeval chars/sec. is the rate.
# Part 0 is a no-op for 1 sec., same as the audio dead space.
# In the text, '@' is a space that also marks the end of text to be displayed.
# Nothing at or after the @ is shown in that part.

# Parts starting with "S" are scrollers
#IDX_LOADING = 0
#IDX_HEY = 2
#IDX_FALKEN = 3
#IDX_XPORT = 6

message_human = """\
Blank/0/@
LogoFadeIn/0/@
Intro/0/     INCLINe Is bACk wIth <<< MULtIChANNEL sOUNd >>>@@@
LogoFadeOut/0/@
Main/0/        ThaNk yOU for sUppORtINg the NORth AMeRICAN deMOsCeNe!@@@@
Greetz/0/       GReetIngs tO ALL pARtyPeOPLe      ANd tO eVeRy POLISH PERSON In ThE worLd ... INdIvIduaLLY aNd In ALPHAbetIcAL ORdER!@@@@
Credz/0/        ALsO tO bf bRs cmucc cvgm NCe tpOLM <> NCL02 - COde cxw ^ phRaNk - musIc MakRO et aL. - logo by specIaL gUest Raven^NCE         Text Repeats@@@@@
Endpart/0/@"""
#trad. aRR. KAzIMIeRz SIkORskI
#------------------------------------------
# Font

def m(*segs):
    """ make a binary mask from the segments in the input iterable """
    rv=0
    for s in segs:
        rv += 2**s
    return rv

#     00000
#   1   2   3
#   1   2   3
#   1   2   3
#     44444
#   5       6
#   5       6
#  858      6
#  88877777
#  888

CHARS={
    # Single segments, for debugging.  Bang is handled separately.
    #')':1, '!':2, '@':4, '#':8, '$':16, '%':32, '^':64, '&':128,
    # Combined, also for debugging
    '*':255,
    # Punctuation
    ' ':0,
    '.':m(8),
    '!':m(1,8),
    '?':m(0,3,4,8),
    '-':m(4),
    '(':m(0,1,5,7),
    ')':m(0,3,6,7),
    '<':m(3,4,6),       # well, barely, but hey...
    '>':m(1,4,5),       # ditto
    '\'':m(2),
    '|':m(2),
    '_':m(7),
    '@':0,  # express blank, plus marks the clip char.
    ',':m(5),
    "'":m(1),
    '`':m(3),
    '^':m(2,4),         # why not?
    # Alphanumerics
    '0':m(0,1,3,5,6,7),
    '1':m(3,6),
    '2':m(0,3,4,5,7),
    '3':m(0,3,4,6,7),
    '4':m(1,3,4,6),
    '5':m(0,1,4,6,7),
    '6':m(0,1,4,5,6,7),
    '7':m(0,3,6),
    '8':m(0,1,3,4,5,6,7),
    '9':m(0,1,3,4,6),
    # alpha
    'A':m(5,1,0,3,6,4),
    'a':m(5,1,0,3,6,4),
    'B':m(1,5,7,6,4),
    'b':m(1,5,7,6,4),
    'C':m(0,1,5,7),
    'c':m(0,1,4),
    'D':m(3,4,5,6,7),
    'd':m(4,5,7,6,3),
    'E':m(0,1,4,5,7),
    'e':m(4,3,0,1,5,7),
    'F':m(0,1,4,5),
    'f':m(0,1,4,5),
    'G':m(4,1,0,3,6,7),
    'g':m(4,1,0,3,6,7),
    'H':m(1,3,4,5,6),
    'h':m(1,4,5,6),
    'I':m(0,2,4),
    'i':m(1),   # TODO 3?  3,6?
    'J':m(3,6,7),
    'j':m(3,6,7),
    'K':m(1,2,4,5,6),
    'k':m(1,2,4,5,6),
    'L':m(1,5,7),
    'l':m(1,4), # TODO 3,6?  1,5,7?
    'M':m(0,1,2,3,5,6),
    'm':m(0,1,2,3),
    'N':m(0,1,3,5,6),
    'n':m(0,1,3),
    'O':m(0,1,5,7,6,3),
    'o':m(0,1,3,4),
    'P':m(0,1,3,4,5),
    'p':m(0,1,3,4,5),
    'Q':m(0,1,3,4,6),
    'q':m(0,1,3,4,6),
    'R':m(0,1,5),
    'r':m(0,1),
    'S':m(0,1,4,6,7),
    's':m(0,1,4,6,7),
    'T':m(1,4,5,7),
    't':m(1,4,5,7),
    'U':m(1,5,7,6,3),
    'u':m(1,3,4),       # let context distinguish it from 'v'
    'V':m(1,5,7,6,3),   # TODO special-case?
    'v':m(1,4,3),
    'W':m(1,2,3,4),
    'w':m(1,2,3,4),
    'X':m(2,3,4,5,6),   # TODO special-case it.
    'x':m(2,3,4,5,6),
    'Y':m(1,3,4,6,7),
    'y':m(1,3,4,6,7),
    'Z':m(0,3,4,5,7),
    'z':m(0,3,4,5,7),
}   # CHARS

############################################################################
import pdb
import re
from textwrap import indent, dedent

# Constants
VECSZ = 4   # for make_vec4s

# Helpers

from util import *

def make_vec4s(msg, PAD=' '):
    """ Helper for print_fullmessage_bst.  Makes vec4s holding the elements
    of character array _msg_, grouped by 4.  Padding is PAD."""

    # Pad to a multiple of VECSZ characters
    extrachars = len(msg) % VECSZ
    extrachars = 0 if extrachars==0 else VECSZ-extrachars
    msg = msg + (PAD*extrachars)

    # Build the groupings
    retval = []
    for group_start_idx in range(0,len(msg),VECSZ):
        grp = msg[group_start_idx:group_start_idx+VECSZ]
        # Convert the chars in _grp_ to the appropriate float mask values
        codes=[]
        for (chidx,ch) in enumerate(grp):
            if not (ch in CHARS):
                raise KeyError("Char %s @ %d/%d not in CHARS"%
                                (ch,group_start_idx+chidx,len(msg)))
            codes.append("%.1f"%CHARS[ch])
        # next ch
        # Pack the floats into a vec4(...) string
        retval.append("vec4(%s)"%','.join(codes))
    #next group_start_idx
    return retval

# make_vec4s()

def print_fullmessage_bst_helper(vec4s, st, en, level=1):
    """ Print an if/then/else BST of msg.  st is the first index to print
    and en is the last index + 1.  This makes a _huge_ difference to speed
    compared to a linear lookup over fullmessage.

    To reduce the depth of the if/then tree, groups of four adjacent characters
    are stuffed into vec4s, and a mix/step sequence at the end picks the
    correct element of the vec4.  Padding is PAD."""
    THLD = 4    # below this many vec4s, do it linear

    # Body
    if (en-st) > THLD:  # Recursive case
        mid = st + (en-st)//2   # integer divide
        print(id4("if(vecidx>=%.1f){"%mid, level))     # %.1f because float
        print_fullmessage_bst_helper(vec4s, mid, en, level+1)
        print(id4("}else{",level))
        print_fullmessage_bst_helper(vec4s, st, mid, level+1)
        print(id4("}",level))

    else:               # base case
        for idx in reversed(range(st, en)):
            print(id4("""if(vecidx>=%.1f) return %s;"""%(idx, vec4s[idx]), level))
    #endif recurse else

# print_fullmessage_bst_helper()

def print_full_message(msg):
    # Helper header
    print(dedent("""\
        vec4 get_seg_vec4(float vecidx) {"""))

    # Helper body as a binary search tree
    vec4s = make_vec4s(msg)     # Group into 4s
    print_fullmessage_bst_helper(vec4s, 0, len(vec4s))

    # Helper footer
    print(dedent("""\
            return vec4(0.0);
        } //get_seg_vec4
        """))

    # Main routine
    print(dedent("""\
        #define NUM_CHARS_IN_MESSAGE (%.1f)
        float get_seg_mask(float charidx)
        {
            if(charidx>=NUM_CHARS_IN_MESSAGE) return 0.0; //blank at the end
            float vecidx = charidx * %.9f;
            float subidx = mod(charidx, %.1f);
            vec4 v = get_seg_vec4(vecidx);
            float rv = v[0];
            rv = mix(rv, v[1], step(1.0, subidx));
            rv = mix(rv, v[2], step(2.0, subidx));
            rv = mix(rv, v[3], step(3.0, subidx));
            return rv;
        } //get_seg_mask
        """ % (len(msg), 1.0/VECSZ, VECSZ) ))

# print_full_message

##########################################################################
# Main

#------------------------------------------
# Preprocess the message

NONPRINT = re.compile(r'[^a-z0-9_]', re.IGNORECASE)

message=[x.split('/') for x in message_human.split('\n')]

## Pad each row with some spaces so they won't overlap with the previous part?
#for idx, x in enumerate(message):
#    x[2] = 4*' ' + x[2]
#    message[idx] = x
##next x

# Parse out the pieces of the script
times=[float(x[1]) for x in message]    # duration of each part
partnames=[]
starttimes=[0.0]    # start time of each part
startcharidxes=[]
fullmessage=""
charcounts=[len(x[2]) for x in message]
rate_cps=[] # Chars per second in each part
clipcharidxes=[]

# Get the timing overrides, if any, from the music.
start_secs=[]
try:
    with open('msx-part-boundaries.csv','r', newline='') as fd:
        rdr = csv.reader(fd, quoting=csv.QUOTE_NONNUMERIC)
        start_secs = [x[0]/BPS for x in rdr]
            # freeze the list since fd is going away after this line.
            # also grab element 0 in each row, which is the beat, and
            # convert to seconds.
except FileNotFoundError:
    print("// No music sync file found")
    pass

if len(start_secs) == (len(times)+1):
    # We have all the start and end times from the music.
    print("gen-gfx: Synced to music OK", file=sys.stderr)
    print("// Syncing to music: %s"%str(start_secs))
    starttimes = start_secs[:-1]
    endtimes = start_secs[1:]
    times = [e-st for e,st in zip(endtimes,starttimes)]
        # duration of each part
    rate_cps = [nch/dur for nch,dur in zip(charcounts,times)]

else:
    # We don't have start and end times from the music, so
    # regularize the times and compute other useful things ourselves.
    print("// No music sync found")
    for idx,t in enumerate(times):
        if t==0:    # use the default rate
            times[idx]=charcounts[idx]/rate    # float div
            rate_cps += [rate]
        elif t<0:   # negative of the rate was provided
            times[idx]=charcounts[idx]/(-t)
            rate_cps += [-t]
        else:       # time in seconds - compute the rate from the time
            rate_cps += [charcounts[idx]/times[idx]]    # float div

        if idx>0:
            starttimes += [sum(times[:idx])]
    # next idx,t

    # The end times are shifted 1 from the start times
    endtimes = starttimes[1:]
    endtimes += [sum(times)]

# endif sync else

for idx,t in enumerate(times):
    startcharidxes += [len(fullmessage)]
    fullmessage += message[idx][2]
    partnames += [NONPRINT.sub('',message[idx][0].replace(' ','_')).upper()]

    # Clip char: no text shown from that point on.
    if '@' in message[idx][2]:  # Explicit: the first '@' in the text
        clipcharidxes += [startcharidxes[idx]+message[idx][2].find('@')]
    else:                       # Implicit: the off-the-end char of the
        clipcharidxes += [len(fullmessage)]     # part's text.
    #endif
# next idx,t

# Story -----
# Part numbers and start times
print("// Parts and start times")
for idx, name in enumerate(partnames):
    print("#define %s (%.1f)"%(name,idx))
    print("#define %s_START (%.20f)"%(name,starttimes[idx]))
#next

# Header
print(dedent("""
    vec4 get_story(in float time)
    {   //returns vec4(partnum, charidx_frac, first_charidx, clip_charidx)
        // NOTE: charidx_frac restarts at 0 each part!
        // first_charidx and clip_charidx are with respect to the whole messge.
        // Character indices starting with clip_charidx should not be displayed.
        float partnum, charidx_frac, first_charidx, clip_charidx;"""
    ))

# Each part
# TODO: always print 0 as 0.0.  For now, use %.20f as a workaround.
for idx, tend in enumerate(endtimes):
    print(id4("""\
    if(time<%.20f) {
        partnum=%s;
        charidx_frac=(time-%s_START)*%.20f;
        first_charidx=%.1f;
        clip_charidx=%.1f;
    } else
    """%(tend, partnames[idx], partnames[idx],
        rate_cps[idx] if rate_cps[idx]>0 else 1,
        startcharidxes[idx], clipcharidxes[idx])))
#next idx,tstart

# Footer
print(id4("""\
    {
        partnum=0.0;
        charidx_frac=0.0;
        first_charidx=0.0;
        clip_charidx=0.0;
    }
    """))
print(dedent("""\
        return vec4(partnum,charidx_frac,first_charidx,clip_charidx);
    } //get_story
    """
    ))


# Text -----
print_full_message(fullmessage)

# Light and camera dispatcher -----
print(dedent("""\
    // Camera and light prototypes
    """))

LC_HDR = "void do_cl_%s(in float partnum, in float charidx_frac, \
out vec3 camera_pos, \
out vec3 camera_look_at, out vec3 camera_up, \
out float fovy_deg, out vec3 light_pos);"

# Generate prototypes
for n in partnames:
    print(LC_HDR%n.lower())

# Dispatcher.  Use one binary split for a touch of speed
mid = len(partnames)//2
print(dedent("""
    void do_camera_light(in float partnum, in float charidx_frac,
                            out vec3 camera_pos,
                            out vec3 camera_look_at, out vec3 camera_up,
                            out float fovy_deg, out vec3 light_pos)
    {   // Camera and light dispatcher
        if(partnum>=%s) {
    """%partnames[mid]))


for n in partnames[mid:len(partnames)]:
    print(id4("""\
        if(partnum==%s) {
            do_cl_%s(partnum,charidx_frac,camera_pos,camera_look_at,camera_up,fovy_deg,light_pos);
        } else
        """%(n, n.lower()), level=2))
#next n

print(id4("""\
        {
            camera_pos=vec3(0.0,0.0,10.0);    //default
            camera_look_at=vec3(0.0);
            camera_up=vec3(0.0, 1.0, 0.0);
            fovy_deg=45.0;
            light_pos=camera_pos;
        }
    } else {
    """))

for n in partnames[0:mid]:
    print(id4("""\
        if(partnum==%s) {
            do_cl_%s(partnum,charidx_frac,camera_pos,camera_look_at,camera_up,fovy_deg,light_pos);
        } else
        """%(n, n.lower()), level=2))
#next n

print(dedent("""\
            {
                camera_pos=vec3(0.0,0.0,10.0);    //default
                camera_look_at=vec3(0.0);
                camera_up=vec3(0.0, 1.0, 0.0);
                fovy_deg=45.0;
                light_pos=camera_pos;
            }
        }
    } //do_camera_light
    """))

## Hacks: four spaces before the first char in each of these lines
#print("#define HEY_REALSTART (%.20f)"%(startcharidxes[IDX_HEY]+4));
#print("#define FALKEN_REALSTART (%.20f)"%(startcharidxes[IDX_FALKEN]+4));
#
## Transporter
#print("#define XPORT_NCHARS (%.20f)" %
#        (clipcharidxes[IDX_XPORT]-startcharidxes[IDX_XPORT])
#);
#print("#define LOADING_NCHARS (%.20f)" %
#        (clipcharidxes[IDX_LOADING]-startcharidxes[IDX_LOADING])
#);
##print("#define XPORT_FIRST_CHARIDX (%.20f)" % startcharidxes[IDX_XPORT]);
##print("#define XPORT_CLIP_CHARIDX (%.20f)" % clipcharidxes[IDX_XPORT]);
#print("#define XPORT_FADEIN_DURATION (5.0)");

# vi: set ts=4 sts=4 sw=4 et ai: #

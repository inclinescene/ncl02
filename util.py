# util.py: Utility routines
from textwrap import indent, dedent

def id4(s, level=1):
    """ print _s_ with a four-space indent """
    return indent(dedent(s), prefix=(level*4)*' ')
#id4()

# vi: set ts=4 sts=4 sw=4 et ai: #

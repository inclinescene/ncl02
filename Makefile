# Makefile for ncl02 by cxw/Incline

TARGETS=gfx.frag msx.frag
SYNC=msx-part-boundaries.csv
DEPS=music.frag.inc gen-msx.py gen-msx-credits.py gen-gfx.py $(SYNC)

PPFLAGS=
	# set to -Dgs for glsl sandbox

export SHELL=/bin/bash

all: $(TARGETS)
	for f in $(TARGETS) ; do \
		cat "$$f" | putclip ; \
		echo "Paste $$f where it goes then hit Enter" ; \
		read ; \
	done

#gs: $(TARGET_GS)
#
#$(TARGET_GS): ncl02.frag.in $(DEPS)
#	perlpp -Dgs -o $@ $<
#	cat "$@" | putclip

# Dependency order.  TODO: be more sophisticated about this.
$(TARGETS): $(DEPS)
$(SYNC): gen-msx.py
gen-gfx.py: $(SYNC)

# Run the music first to set up the sync.
gfx.frag: msx.frag

# Rules
# preprocess files with perlpp from https://github.com/cxw42/perlpp,
# which is forked from https://github.com/d-ash/perlpp.
# This rule modified from http://stackoverflow.com/a/397232/2877364 by
# http://stackoverflow.com/users/49301/nathan-kurz .
% :: %.in
	perlpp $(PPFLAGS) -o $@ $<
# NOTE: the gen-*.py calls are done by perlpp

# Convenience rules for regenerating only one shader
g: gfx.frag
	cat "$<" | putclip ; echo "Paste $< where it goes."

m: msx.frag
	cat "$<" | putclip ; echo "Paste $< where it goes."

# vi: set ts=8 sts=0 sw=8 noet ai ff=unix: #

